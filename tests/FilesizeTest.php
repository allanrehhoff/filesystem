<?php
	class FilesizesTest extends \PHPUnit\Framework\TestCase {
		public function setUp() {
		}

		public function testAutoloaderWorks() {
			$fs = new \Filesystem\Filesystem;
			$this->assertInstanceOf("Filesystem\Filesystem", $fs);
		}

		public function testBinaryDehumanizePrefix() {
			$dehumanized = Filesystem\Filesize::dehumanize("1KiB");
			$this->assertEquals(1024, $dehumanized);
		}

		public function testBinaryHumanizedPrefix() {
			$humanized = Filesystem\Filesize::humanize(3072, 2, true);
			$this->assertEquals("3KiB", $humanized);
		}

		public function testMetricDehumanize() {
			$dehumanized = Filesystem\Filesize::dehumanize("1MB");
			$this->assertEquals(1000000, $dehumanized);
		}

		public function testMetricHumanize() {
			$humanized = Filesystem\Filesize::humanize(3000000);
			$this->assertEquals("3MB", $humanized);
		}
	}