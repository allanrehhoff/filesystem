#Filesystem Library for common tasks#

Contains two utility classes for dealing with files and folders easily, and reducing redundant code.  
Please inspect the source functions for further documentation.  

If your application already includes an autoloader which supports namespaces, simple copy the Filesystem/ folder to your vendor or classes directory.  
otherwise include/require the bundled autoloader.  