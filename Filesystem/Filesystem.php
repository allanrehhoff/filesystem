<?php
namespace FileSystem {
	class FileSystem {

		/**
		* Read the contents of a file, using an exclusive file lock handle.
		* @param string $file The file to read.
		* @throws Filesystem Exception
		* @return mixed
		*/
		public static function readfileExclusive(string $file) {
			$handle = fopen($file, 'r');
			$locked = flock($f, LOCK_EX);

			if($locked === false) {
				throw new FilesystemException("Could not acquire an exclusive lock on file.");
			}
			
			$content = fread($handle, filesize($file));

			flock($handle, LOCK_UN);			
			fclose($handle);
			
			return $content;
		}

		/**
		* Traverse recursively through a directory using a callback function, skipping dot directories.
		* @param string $directory Directory to traverse recursively.
		* @param callable $callback Callback to apply to each item.
		* @throws FilesystemException
		* @return void
		*/
		public static function forEachFile(string $directory, callable $callback) {
			if(!is_dir($directory)) {
				throw new FilesystemException("$directory No such file or directory, refusing to continue.");
			}

			foreach(scandir($directory) as $item) {
				if(in_array($item, ['.','..'])) continue;

				$absItem = $directory.'/'.$item;

				if(is_file($absItem)) {
					$callback($absItem);
				} else if(is_dir($absItem)) {
					self::forEachFile($absItem, $callback);
				}
			}
		}
	}
}